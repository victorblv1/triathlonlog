class CreateEntries < ActiveRecord::Migration[6.1]
  def change
    create_table :entries do |t|
      t.string :session_type
      t.float :swim
      t.float :bike
      t.float :run

      t.timestamps
    end
  end
end
