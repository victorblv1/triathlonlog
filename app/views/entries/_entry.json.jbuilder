json.extract! entry, :id, :session_type, :swim, :bike, :run, :created_at, :updated_at
json.url entry_url(entry, format: :json)
