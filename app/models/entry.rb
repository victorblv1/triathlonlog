class Entry < ApplicationRecord
  belongs_to :category
  validates :session_type, :swim, :bike, :run, :category_id, presence: true

  def day
    self.created_at.strftime('%b %e, %Y')
  end
end
