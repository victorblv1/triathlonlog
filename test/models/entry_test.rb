require "test_helper"

class EntryTest < ActiveSupport::TestCase
  test 'is valid with valid attributes' do
    entry = Entry.new(session_type: 'Evening Session', swim: 1.02, bike: 9.01, run: 7.01)
    assert entry.save
  end
end
