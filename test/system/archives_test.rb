require "application_system_test_case"

class ArchivesTest < ApplicationSystemTestCase
  setup do
    @entry = entries(:morningSession)
  end

  test "visiting the archives index" do
    visit archives_index_url

    assert_selector "h1", text: "TriathlonLog"
    assert_selector "h3", text: "Entries for #{@entry.day}"
    assert text: @entry.session_type
  end
end
